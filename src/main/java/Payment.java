import java.util.Objects;

//Создайте класс Payment (платеж) с полями: ФИО человека (одна строка), дата
//платежа — число, месяц и год (целые числа), сумма платежа (целое число — сумма
//в копейках). Напишите необходимые конструкторы, геттеры/сеттеры, методы
//equals, hashCode, toString.

public class Payment extends BankUser implements Cloneable{
    private int payment;

    public Payment(String name, String date, int payment){
        super(name, date);
        this.payment = payment;
    }
    public Payment(String name, int day, int month, int year, int payment){
        super(name, day,month,year);
        this.payment = payment;
    }

    public int getPayment(){
        return payment;
    }
    public void setPayment(int payment){
        this.payment = payment;
    }
    public Date getDate(){
        return super.getDate();
    }

    @Override
    public String toString() {
        return String.format(
                "Плательщик: %s ,дата: %d.%d.%d сумма %d рублей %02d копеек",
                super.getName(),
                super.getDate().getDay(), super.getDate().getMonth(),super.getDate().getYear(),
                payment/100,payment%100
        );
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Payment newPayment = (Payment) super.clone();
        newPayment.payment = this.payment;
        return newPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment1 = (Payment) o;
        return payment == payment1.payment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(payment);
    }
}
