import java.util.Arrays;

//Создайте класс FinanceReport, содержащий массив платежей, ФИО составителя
//отчета, дату создания отчета. Методы: получение количества платежей, доступ к iму платежу (на чтение и запись).

public class FinanceReport extends BankUser implements Cloneable{
    private Payment[] payments;

    public FinanceReport(String name, Date date, Payment[] payments){
        super(name, date);
        this.payments = payments;
    }

    public FinanceReport(String name, String date, Payment[] payments){
        this(name, new Date(date), payments);
    }

    public FinanceReport(String name, int day, int month, int year, Payment[] payments){
        this(name, new Date(day,month,year), payments);
    }

    //Добавьте конструктор копирования (после создания копии массива при изменении
    //данных в объектах исходного массива копия изменяться не должна).
    public FinanceReport(FinanceReport other){
        super(other.getName(), other.getDate());
        this.payments = new Payment[other.length()];
        for(int i = 0;i < this.length();++i){
            this.payments[i] = new Payment(other.getPayments()[i].getName(),
                    other.getPayments()[i].getDate().getDay(),
                    other.getPayments()[i].getDate().getMonth(),
                    other.getPayments()[i].getDate().getYear(),
                    other.getPayments()[i].getPayment());
        }
    }

    public Payment paymentAt(int index){
        return payments[index];
    }

    public Payment[] getPayments(){
        return payments;
    }

    public int length(){
        if(payments != null){
            return payments.length;
        } else {
            return 0;
        }
    }

    //Добавьте в FinanceReport метод toString, который преобразует отчет в набор строк
    //формата (используйте String.format):
    //[Автор: ФИО составителя, дата: дата.создания, Платежи: [
    // Плательщик: ФИО, дата: день.месяц.год сумма: *** руб. ** коп.\n,
    // Плательщик: ФИО, дата: день.месяц.год сумма: *** руб. ** коп.\n,… ]]
    @Override
    public String toString() {
        return "Finance Report{" + super.toString() + "\npayments" + Arrays.toString(payments) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinanceReport that = (FinanceReport) o;
        return Arrays.equals(payments, that.payments);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(payments);
    }
}
