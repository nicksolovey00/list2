import org.junit.Assert;
import org.junit.Test;

public class TestFinances {
    Payment payment1 = new Payment("J.J Jonson", 9, 11, 1999, 200);
    Payment payment2 = new Payment("J.J Manson", 3, 1, 1987, 100);
    Payment payment3 = new Payment("J.J Hubert", 2, 5, 1959, 200);

    FinanceReport financeReport = new FinanceReport("Julia", new Date(8, 11, 2011),
            new Payment[]{
                    payment1,
                    payment2,
                    payment3
            });

    @Test
    public void testFinanceReportToString(){
        System.out.println(financeReport);
    }

    @Test
    public void testCopyConstructor() throws CloneNotSupportedException {
        FinanceReport financeReport2 = new FinanceReport(financeReport);
        Assert.assertEquals(financeReport2, financeReport);
        financeReport2.getPayments()[0].setPayment(78);
        Assert.assertNotEquals(financeReport2, financeReport);
    }

    @Test
    public void testFinanceReportProcessor(){
        Payment[] expected = new Payment[]{
                payment1,
                payment2,
                payment3
        };
        Assert.assertArrayEquals(expected, FinanceReportProcessor.getReportBySurname(financeReport, "J.J"));
        expected = new Payment[]{
                payment2
        };
        Assert.assertArrayEquals(expected, FinanceReportProcessor.getReportByPayment(financeReport, 160));

    }
}
